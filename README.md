mark/jump for bash
===================

* Usable with Dredhorse bash-it
* Install with ```homesick clone https://dredhorse@bitbucket.org/dredhorse/homesick-bash-mark_jump.git```
* Based on http://jeroenjanssens.com/2013/08/16/quickly-navigate-your-filesystem-from-the-command-line.html
* supported OS: MacOS, Ubuntu. Others untested.

Usage
=======

```
$ cd ~/some/very/deep/often-used/directory
$ mark deep
```

This adds a symbolic link named deep to the directory ~/.marks. To jump to this directory, type the following from any place in the filesystem:

```
$ jump deep
```

To remove the bookmark (i.e., the symbolic link), type:

```
$ unmark deep
```

You can view all marks by typing:

```
$ marks

deep    -> /home/johndoe/some/very/deep/often-used/directory
foo     -> /usr/bin/foo/bar
```


Install
========

Automatic Install using [DRedhorse Homeshick] [1] 
```
homeshick clone https://dredhorse@bitbucket.org/dredhorse/homesick-bash-mark_jump.git
```

Manual install with normal Homeshick or Homesick

```
homesick clone https://dredhorse@bitbucket.org/dredhorse/homesick-bash-mark_jump.git
~/.homesick/repos/homesick-bash-mark_jump/install
```

[1]: https://github.com/dredhorse/homeshick.git "DRedhorse Homeshick" 
